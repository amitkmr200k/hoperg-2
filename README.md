# Hope RG Assignment 2 | User-Posts App

## Getting Started

### Prerequisites

Please make sure you have following installed in your machine.

* Node
* npm
* typescript

If not, please install them before moving forward.

Commands to install above required items on Ubuntu sytsem:

1) Install Node and npm
```
sudo apt-get install nodejs
```
2) Install typescript via npm
```
npm install -g typescript
```

### Project Steup

Follow the setps given below to setup the project

Steps to follow:
```
1) Create and project directory and checkout to created project directory
mkdir hope-rg-new
cd hope-rg-new

2) Clone the repository in project directory
https://gitlab.com/amitkmr200k/hoperg-2.git .

3) Open constants file located at src/constants.ts
and do the changes as required.

4) From project directory run following commands:
npm install
tsc

5) To run the 2 scripts(user and posts), run the following commands from project Dir
node dist/scripts/users.js
node dist/scripts/posts.js

6) To run the project, run the following command:
npm start

7) Now the project is up and running, and you can now make API calls.
```
## API EndPoints


Below are the endpoints for making API calls.

```
Fetching Users:
Method: Get
API endpoint: http://localhost:3000/api/user
```

```
Fetching Posts:
Method: Get
API endpoint: http://localhost:3000/api/user/posts
```

```
Uploading Image of User (file Upload):
Method: POST
API endpoint: http://localhost:3000/api/user/:userId/image
```