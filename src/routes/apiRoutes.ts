import { Router, Request, Response } from 'express';
import * as UserController from '../controllers/userController';
import * as multer from 'multer';

const router: Router = Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './uploads/');
    },
    filename: (req, file, cb) => {
        let fileExtension = file.mimetype.split('/')[1];
        cb(null, 'user-' + req.params.userId + '.' + fileExtension);
    }
});

function fileFilter (req, file, cb) {
    if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
        cb(null, true);
    } else {
        cb(null, false)
    }
}

const upload = multer({storage: storage, fileFilter: fileFilter});

router.get('/user', UserController.fetchUsers);
router.get('/user/posts', UserController.fetchPosts);
router.post('/user/:userId/image', upload.single('image'), UserController.uploadImage);

export const apiRoutes: Router = router;
