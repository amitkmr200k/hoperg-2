import * as mongoose from 'mongoose';
import * as https from 'https';
import {constants} from '../constants';
import { UserSchema } from '../models/users';

https.get(constants.USERS_API_ENDPOINT, (res) => {
    const { statusCode } = res;
    const contentType = res.headers['content-type'];

    let error;
    if (statusCode !== 200) {
        error = new Error('Request Failed.\n' +
                        `Status Code: ${statusCode}`);
    } else if (!/^application\/json/.test(contentType)) {
        error = new Error('Invalid content-type.\n' +
                        `Expected application/json but received ${contentType}`);
    }
    if (error) {
        console.error(error.message);
        return;
    }

    res.setEncoding('utf8');
    let rawData = '';
    res.on('data', (chunk) => { rawData += chunk; });

    res.on('end', async () => {
        try {
            const parsedData = JSON.parse(rawData);

            for(let data of parsedData) {
                data['sourceUserId'] = data['id'];

                await saveUser(data);

            }
        } catch (e) {
            console.error(e.message);
            return;
        }
    });
}).on('error', (e) => {
        console.error(`Got error: ${e.message}`);
        return;
});

/**
 * Function to connect to mongoDB
 *
 * @param null
 *
 * @return Promise Object
 */
function connectToMongoDB() {
    return new Promise(async (resolve, reject) => {
        try {
            const uri: string = constants.MONGODB_URL + constants.MASTER_DB;

            mongoose.connect(uri, { useNewUrlParser: true },(err: any) => {
                if (err) {
                    console.log(err.message);
                    reject(err);
                } else {
                    console.log("Succesfully Connected!");
                    resolve();
                }
            });
        } catch (error) {
            reject(error);
        } finally {
            if (mongoose.Connection.Status) {
                try {
                    await mongoose.disconnect();
                    console.log('Connection closed');
                    resolve();
                } catch (err) {
                    reject(err);
                    console.log('Error closing connection', err);
                }
            }
        }
    });

}


/**
 * Function to save User data
 *
 * @param data Obejct
 *
 * @return Promise Object
 */
function saveUserData(data: Object) {
    return new Promise(async (resolve, reject) => {
        try {
            await connectToMongoDB();

            const User = mongoose.model(constants.USERS_COLL, UserSchema);

            User.findOneAndUpdate({'sourceUserId': data['sourceUserId']}, data, (updateErr, updatedUser) => {

                if(updateErr){
                    console.log(updateErr);
                    reject(updateErr);
                }

                if(!updatedUser) {
                    let newUser = new User(data);
                    newUser.save((createErr, userCreated) => {
                        if(createErr){
                            console.log(createErr);
                            reject(createErr);
                        }

                        console.log(`User ${data['sourceUserId']} added Successfully`);
                        resolve('success');
                    });
                } else {
                    console.log(`User ${data['sourceUserId']} updated Successfully`);
                    resolve('success');
                }
            });
        } catch (error) {
            reject(error);
        }
    });
}

/**
 * Async Function to call saveUserData function to save User data
 *
 * @param data Obejct
 *
 * @return null
 */
async function saveUser(data: Object) {
    let user = await saveUserData(data);

    console.log(user);
}


