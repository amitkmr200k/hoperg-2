import * as mongoose from 'mongoose';
import * as https from 'https';
import {constants} from '../constants';

import { UserSchema } from '../models/users';
import { PostSchema } from '../models/posts';

import { resolve } from 'url';

getPostsAndComments();

async function getPostsAndComments() {

    let posts = await getDataFromSource('posts');
    let comments = await getDataFromSource('comments');

    let postsSaved = await savePosts(posts, comments);

    return;
}

async function savePosts(data, comments) {
    for(let i = 0; i < data.length; i++) {
        let posts = await savePostsData(data[i], comments);
    }
}

function getDataFromSource(resource: String) {
    return new Promise(async (resolve, reject) => {
        try {
            https.get(constants.API_ENDPOINT + resource, (res) => {
                const { statusCode } = res;
                const contentType = res.headers['content-type'];

                let error;
                if (statusCode !== 200) {
                    error = new Error('Request Failed.\n' +
                                    `Status Code: ${statusCode}`);
                } else if (!/^application\/json/.test(contentType)) {
                    error = new Error('Invalid content-type.\n' +
                                    `Expected application/json but received ${contentType}`);
                }
                if (error) {
                    console.error(error.message);
                    return;
                }

                res.setEncoding('utf8');
                let rawData = '';
                res.on('data', (chunk) => { rawData += chunk; });

                res.on('end', () => {
                    try {
                        const parsedData = JSON.parse(rawData);
                        resolve(parsedData);
                    } catch (e) {
                        console.error(e.message);
                        reject(e);
                    }
                });
            }).on('error', (e) => {
                console.error(`Got error: ${e.message}`);
                reject(e);
            });
        } catch(e) {
            console.error(`Got error: ${e.message}`);
            reject(e);
        }
    });
}

function savePostsData(postsData, commentsData) {
    return new Promise(async (resolve, reject) => {
        try {
            if(postsData['userId'] != undefined) {
                let postComments = commentsData.filter(item => item.postId === postsData['id']);

                postsData['sourceUserId'] = postsData['userId'];
                postsData['sourcePostId'] = postsData['id'];

                for (let postComment of postComments) {
                    postComment['sourcePostId'] = postComment['postId'];
                    postComment['sourceCommentId'] = postComment['id'];

                    delete postComment['postId'];
                    delete postComment['id'];
                }

                postsData['comments'] = postComments;

                let uri: string = constants.MONGODB_URL + constants.USER_DB + postsData['userId'];

                mongoose.connect(uri, { useNewUrlParser: true }, (mongoErr: any) => {
                    if (mongoErr) {
                        console.log(mongoErr.message);
                        reject(mongoErr.message);
                    } else {
                        console.log("Succesfully Connected!");
                        const Post = mongoose.model(constants.POSTS_COLL, PostSchema);

                        Post.findOneAndUpdate({'sourcePostId': postsData['id']}, postsData, (updateErr, updatedPost) => {
                            if(updateErr){
                                console.log(updateErr);
                                reject(updateErr);
                            }

                            if(!updatedPost) {
                                let newPost = new Post(postsData);
                                newPost.save((createErr, userCreated) => {
                                    if(createErr){
                                        console.log(createErr);
                                        reject(createErr);
                                    }

                                    console.log(`Post ${postsData['sourcePostId']} added Successfully`);
                                    resolve(postsData);
                                });
                            } else {
                                console.log(`Post ${postsData['sourcePostId']} updated Successfully`);
                                resolve(postsData);
                            }

                        });
                    }
                });
            } else {
                console.log('Post with id: ' + postsData['id'] + ' is not valid');
                resolve(postsData);
            }
        } catch(e) {
            console.error(`Got error: ${e.message}`);
            reject(e);
        } finally {
            if (mongoose.Connection.Status) {
                try {
                  await mongoose.disconnect();
                  console.log('Connection closed');
                } catch (err) {
                  console.log('Error closing connection', err);
                }
            }
        }
    });
}
