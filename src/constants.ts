const PORT = 3000;

export const constants = {
    PORT: PORT,
    MONGODB_URL: 'mongodb://127.0.0.1:27017/',
    IMAGE_UPLOAD_PATH: 'uploads',
    MASTER_DB: 'master',
    USER_DB: 'user-',
    USERS_COLL: 'users',
    POSTS_COLL: 'posts',
    APP_ENDPOINT: 'http://localhost:' + PORT + '/',
    API_ENDPOINT: 'https://jsonplaceholder.typicode.com/',
    USERS_API_ENDPOINT: 'https://jsonplaceholder.typicode.com/users',
    POSTS_API_ENDPOINT: 'https://jsonplaceholder.typicode.com/posts',
    COMMENTS_API_ENDPOINT: 'https://jsonplaceholder.typicode.com/comments'
};