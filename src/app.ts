import * as express from "express";
import {apiRoutes} from './routes/apiRoutes';
import {constants} from './constants';

const app = express();

app.use(express.json());

app.set('port', constants.PORT);

app.use('/uploads', express.static('uploads'));

app.use('/api/', apiRoutes);

app.listen(app.get('port'), () => {
    console.log('App listening to ' + constants.PORT);
});