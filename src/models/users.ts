import * as mongoose from 'mongoose';

let Schema = mongoose.Schema;

const LocationSchema = new Schema({
    lat: {
        type: String,
        required: 'lat is required'
    },
    lng: {
        type: String,
        required: 'lng is required'
    }
});

const AddressSchema = new Schema({
    street: {
        type: String,
        required: 'Street is required'
    },
    suite: {
        type: String,
        required: 'Suite is required'
    },
    city: {
        type: String,
        required: 'City Id is required'
    },
    zipcode: {
        type: String,
        required: 'ZipCode is required'
    },
    geo: {
        type: [LocationSchema],
        required: 'geo is required'
    }
});

const CopmanySchema = new Schema({
    name: {
        type: String,
        required: 'Company Name is required'
    },
    catchPhrase: {
        type: String,
        required: 'Company Catch Phrase required'
    },
    bs: {
        type: String,
        required: 'Company BS is required'
    }
});

export const UserSchema = new Schema({
    sourceUserId: {
        type: Number,
        required: 'User Id is required'
    },
    name: {
        type: String,
        required: 'Name is required'
    },
    username: {
        type: String,
        required: 'UserName Id is required'
    },
    email: {
        type: String,
        required: 'email is required'
    },
    address: {
        type: [AddressSchema],
        required: 'email is required'
    },
    phone: {
        type: String,
        required: 'phone is required'
    },
    website: {
        type: String,
        required: 'website is required'
    },
    company: {
        type: [CopmanySchema],
        required: 'Company is required'
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    image: {
        type: String,
        default: 'uploads/default.jpg'
    }
}, {
    versionKey: false
});