import * as mongoose from 'mongoose';

let Schema = mongoose.Schema;

const CommentsSchema = new Schema({
    sourceCommentId: {
        type: Number,
        required: 'Comment Id is required'
    },
    sourcePostId: {
        type: Number,
        required: 'Post Id is required'
    },
    name: {
        type: String,
        required: 'title is required'
    },
    email: {
        type: String,
        required: 'email is required'
    },
    body: {
        type: String,
        required: 'Body is required'
    },
});

export const PostSchema = new Schema({
    sourceUserId: {
        type: Number,
        required: 'User Id is required'
    },
    sourcePostId: {
        type: Number,
        required: 'Post Id is required'
    },
    title: {
        type: String,
        required: 'title is required'
    },
    body: {
        type: String,
        required: 'Body is required'
    },
    comments: {
        type: [CommentsSchema],
        required: 'comments is required'
    },
    created_date: {
        type: Date,
        default: Date.now
    }
}, {
    versionKey: false
});