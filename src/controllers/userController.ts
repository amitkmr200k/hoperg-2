import { Router, Request, Response } from 'express';
import * as https from 'https';
import * as mongoose from 'mongoose';
import {constants} from '../constants';

import { UserSchema } from '../models/users';
import { PostSchema } from '../models/posts';

/**
 * Function to connect to mongoDB
 *
 * @param db String
 *
 * @return Promise Object
 */
function connectToMongoDB(db: String) {
    return new Promise(async (resolve, reject) => {

            const uri: string = constants.MONGODB_URL + db;

            mongoose.connect(uri, { useNewUrlParser: true }, (err: any) => {
                if (err) {
                    console.log(err.message);
                } else {
                    resolve();
                }
            });
    });
}

/**
 * Function to fetch Users from DB
 *
 * @param null
 *
 * @return Promise Object
 */
function getUsers() {
    return new Promise(async (resolve, reject) => {
        try {
            await connectToMongoDB(constants.MASTER_DB);

            const User = mongoose.model(constants.USERS_COLL, UserSchema);

            User.find({}, '-_id -address._id -address.geo._id -company._id', async (err, users) => {
                if(err) {
                    reject(err);

                }

                resolve(users);
            });

        } catch (error) {
            reject(error);
        }
    });
}

/**
 * Function to fetch User Ids from DB
 *
 * @param null
 *
 * @return Promise Object
 */
function getUserIds() {
    return new Promise(async (resolve, reject) => {
        try {
            await connectToMongoDB(constants.MASTER_DB);

            const User = mongoose.model(constants.USERS_COLL, UserSchema);

            User.find({},'sourceUserId', async (err, users) => {
                if(err) {
                    reject(err);
                }

                resolve(users);
            });
        } catch (error) {
            reject(error);
        }
    });
}

/**
 * Function to fetch User Posts by User Id
 *
 * @param userId number
 *
 * @return Promise Object
 */
function getPosts(userId) {
    return new Promise(async (resolve, reject) => {
        try {
            await connectToMongoDB(constants.USER_DB + userId);

            const Posts = mongoose.model(constants.POSTS_COLL, PostSchema);

            await Posts.find({}, '-_id -comments._id', (err, posts) => {
                if(err) {
                    reject(err);

                }

                resolve(posts);
            });
        } catch (error) {
            reject(error);
        }

    });
}

/**
 * Async Function to fetch all User Posts by User Ids
 *
 * @param data object
 *
 * @return Promise Object
 */
async function getUserPosts(data) {
    let postsData = [];
    for(let i=0; i < data.length; i++) {
        let user = await getPosts(data[i].sourceUserId);
        postsData.push(user);
    }

    return postsData;
}

export const fetchUsers = async (req: Request, res: Response) => {
    let users = await getUsers();

    for (let i in users) {
        users[i]['image'] = constants.APP_ENDPOINT + users[i]['image'];
    }

    return res.json({
        'UserData': users
    });
};

export const fetchPosts = async (req: Request, res: Response) => {
    let userIds = await getUserIds();
    let userPosts = await getUserPosts(userIds);

    return res.json({
        'UserPosts': (userPosts.length) ? userPosts[0]: []
    });
};

export const uploadImage = async (req, res: Response) => {
    if(! req.file) {
        return res.json({
            'error': 'No/Incorrect Image uploaded.'
        });
    } else {
        await connectToMongoDB('master');

        const User = mongoose.model(constants.USERS_COLL, UserSchema);

        User.findOneAndUpdate({'sourceUserId': req.params.userId}, {'image': req.file.path}, (updateErr, updateImg) => {

            if(updateErr){
                console.log(updateErr);
                return res.json({
                    error: updateErr
                });
            }

            if(updateImg) {
                return res.json({
                    'UserData': {
                        'userId': req.params.userId,
                        'image': req.file.path
                    },
                });
            } else {
                return res.json({
                    'error': 'Image not uploaded'
                });
            }
        });
    }
};